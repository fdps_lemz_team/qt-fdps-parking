QT				+=	core gui network websockets

greaterThan(QT_MAJOR_VERSION, 4){
    QT			+=	widgets concurrent printsupport
}

SRC_PATH 		= $$PWD/../src
FDPSLIB_PATH	= $$PWD/../../fdpslib

TARGET 			= 	fdps-parking
TEMPLATE 		= 	app

INCLUDEPATH 	+=	$$PWD/../src
INCLUDEPATH 	+=	$$FDPSLIB_PATH

include(../../qt-solutions/qtsingleapplication/src/qtsingleapplication.pri)

SOURCES     	+= 	$$FDPSLIB_PATH/netutil.cpp \	
                    $$FDPSLIB_PATH/settings_adapter.cpp \
					$$SRC_PATH/main.cpp\
					$$SRC_PATH/MainWindow.cpp \
					$$SRC_PATH/ParkingDialog.cpp \
					$$SRC_PATH/ParkingGraphicsItem.cpp \
					$$SRC_PATH/ParkingTableModel.cpp \
					$$SRC_PATH/UnatedParkingGraphicsItem.cpp \
					$$SRC_PATH/WsClient.cpp
	
HEADERS     	+=	$$FDPSLIB_PATH/netutil.h \
                    $$FDPSLIB_PATH/settings_adapter.h \
					$$SRC_PATH/AppSettings.h \
					$$SRC_PATH/ColorSettings.h \
					$$SRC_PATH/MainWindow.h \
					$$SRC_PATH/ParkingDialog.h \
					$$SRC_PATH/ParkingInfo.h\
					$$SRC_PATH/ParkingGraphicsItem.h \
					$$SRC_PATH/ParkingState.h \
					$$SRC_PATH/ParkingTableModel.h \
					$$SRC_PATH/SettsSingle.h \
					$$SRC_PATH/TableViewWithClick.h \
					$$SRC_PATH/UnatedParkingGraphicsItem.h \
					$$SRC_PATH/UnitedParkingInfo.h \
					$$SRC_PATH/UtilWidgetParams.h \
					$$SRC_PATH/UtilWidgetPos.h \
					$$SRC_PATH/WsClient.h \
					$$SRC_PATH/WsProtocol.h \
					$$SRC_PATH/WsClientSettings.h
					
				
RESOURCES   	+=	$$PWD/../icons/icons.qrc
		
FORMS       	+=	$$PWD/../forms/MainWindow.ui \
					$$PWD/../forms/ParkingDialog.ui