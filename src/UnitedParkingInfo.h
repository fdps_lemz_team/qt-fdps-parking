#pragma once

#include <QtCore/QFile>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QMetaType> 
#include <QtCore/QString>
#include <QtCore/QStringList>

#include "ParkingInfo.h"
#include "ParkingState.h"


namespace Fdps {
    namespace parking {

        /// информация о объединенной стоянке
        struct UnitedParkingInfo {
            QString name_;					//!< название стоянки
            ParkingInfoList parkingList_;	//!< парковки
        };

		inline QDataStream & operator<<(QDataStream & out, const UnitedParkingInfo & upi)
		{
			out << upi.name_ << upi.parkingList_;
			return out;
		}

		inline QDataStream & operator>>(QDataStream & in, UnitedParkingInfo & upi)
		{
            in >> upi.name_ >> upi.parkingList_;
			return in;
		}

        using UnitedParkingInfoList = QList<UnitedParkingInfo> ;
    }
}
Q_DECLARE_METATYPE(Fdps::parking::UnitedParkingInfo)
Q_DECLARE_METATYPE(Fdps::parking::UnitedParkingInfoList)