#include <QtCore/qnamespace.h>
#include <QtGui/QCursor>
#include <QtGui/QPainter>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QGraphicsSceneMouseEvent>
#include <QtWidgets/QToolTip>

#include <algorithm>

#include "ParkingDialog.h"
#include "SettsSingle.h"
#include "UnatedParkingGraphicsItem.h"


namespace Fdps {
	namespace parking {

		UnitedParkingGraphicsItem::UnitedParkingGraphicsItem(const UnitedParkingInfo& unitedParkingInfo)
			: QObject()
			, QGraphicsItem()
			, unitedParkingInfo_(unitedParkingInfo)
			, isHighlited_(false) {
			setAcceptHoverEvents(true);
			setCursor(Qt::ArrowCursor);
			setZValue(10);

			std::sort(unitedParkingInfo_.parkingList_.begin(), unitedParkingInfo_.parkingList_.end(),
				[](auto it1, auto it2) {return it1.yPos_ > it2.yPos_; });

			if (unitedParkingInfo_.parkingList_.size()) {
				minCenterX_ = unitedParkingInfo_.parkingList_.first().xPos_;
				minCenterY_ = unitedParkingInfo_.parkingList_.first().yPos_;
				maxCenterX_ = unitedParkingInfo_.parkingList_.last().xPos_;
				maxCenterY_ = unitedParkingInfo_.parkingList_.last().yPos_;
			}

			lenght_ = std::sqrt(std::pow(maxCenterX_ - minCenterX_, 2) + std::pow(maxCenterY_ - minCenterY_, 2));

			angle_ = std::atan(float(maxCenterY_ - minCenterY_) / float(maxCenterX_ - minCenterX_));
			setTransformOriginPoint(QPoint(minCenterX_, minCenterY_));
			setRotation(angle_ * radToDeg);

			for (int nn = 0; nn < unitedParkingInfo_.parkingList_.size(); ++nn) {
				ParkingGraphicsItem* item = new ParkingGraphicsItem(unitedParkingInfo_.parkingList_.at(nn), this);
				item->setRotation(-angle_ * radToDeg);
				connect(item, &ParkingGraphicsItem::parkingInfoChanged, this, &UnitedParkingGraphicsItem::parkingInfoChanged);
				item->setVisible(isHighlited_);
				highlihtedParkingItems_.append(item);
			}

			updateBounds();
		}


		QRectF UnitedParkingGraphicsItem::boundingRect() const {
			float curItemSize = 20.0;

			if (!isHighlited_) {
				return QRectF(minCenterX_ - SettsSingle::instance()->getAppSetts().normalItemSize_ / 2,
					minCenterY_ - SettsSingle::instance()->getAppSetts().normalItemSize_ / 2, 
					lenght_, 
					SettsSingle::instance()->getAppSetts().normalItemSize_);
			} else {
				return QRectF(minCenterX_ - SettsSingle::instance()->getAppSetts().highlightItemSize_ / 2, 
					minCenterY_ - SettsSingle::instance()->getAppSetts().highlightItemSize_ / 2,
					highligtedLenght_ + SettsSingle::instance()->getAppSetts().highlightItemSize_,
					SettsSingle::instance()->getAppSetts().highlightItemSize_ * highlihtedFactor_);
			}
		}


		void UnitedParkingGraphicsItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
			Q_UNUSED(option);
			Q_UNUSED(widget);

			float curItemSize = isHighlited_ ? SettsSingle::instance()->getAppSetts().highlightItemSize_ : 
				SettsSingle::instance()->getAppSetts().normalItemSize_;
			int curFontSize = isHighlited_ ? SettsSingle::instance()->getAppSetts().highlightFontSize_ :
				SettsSingle::instance()->getAppSetts().normalFontSize_;

			float indent;

			if (isHighlited_) {
				setZValue(50);
			} else {
				setZValue(40);
			}

			painter->setBrush(QBrush(QColor("#C8C8C8")));
			painter->setPen(QPen(QBrush(Qt::black), SettsSingle::instance()->getAppSetts().borderWidth_));
			if (!isHighlited_) {
				painter->drawRoundRect(minCenterX_ - SettsSingle::instance()->getAppSetts().normalItemSize_ / 2,
					minCenterY_ - SettsSingle::instance()->getAppSetts().normalItemSize_ / 2, 
					lenght_, 
					SettsSingle::instance()->getAppSetts().normalItemSize_,
					25, 25);
			} else {
				painter->drawRoundRect(minCenterX_ - SettsSingle::instance()->getAppSetts().highlightItemSize_ / 2,
					minCenterY_ - SettsSingle::instance()->getAppSetts().highlightItemSize_ / 2,
					highligtedLenght_ + SettsSingle::instance()->getAppSetts().highlightItemSize_,
					SettsSingle::instance()->getAppSetts().highlightItemSize_ * highlihtedFactor_,
					25, 25);
			}

			if (!isHighlited_) {
				painter->setPen(Qt::black);
				painter->setFont(QFont(SettsSingle::instance()->getAppSetts().fontName_, curFontSize, QFont::Bold, false));
				painter->drawText(minCenterX_ - SettsSingle::instance()->getAppSetts().normalItemSize_ / 2,
					minCenterY_ - SettsSingle::instance()->getAppSetts().normalItemSize_ / 2,
					lenght_, 
					curItemSize,
					Qt::AlignHCenter | Qt::AlignVCenter, 
					unitedParkingInfo_.name_);
			}
		}


		void UnitedParkingGraphicsItem::setParkingRotation(qreal angle) {
			for (auto it : highlihtedParkingItems_) {
				it->setRotation(angle - angle_ * radToDeg);
			}
		}

		void UnitedParkingGraphicsItem::updateBounds() {
			
			for (int nn = 0; nn < highlihtedParkingItems_.size(); ++nn) {
				highlihtedParkingItems_[nn]->setPos(
					minCenterX_ + (nn * ( SettsSingle::instance()->getAppSetts().normalItemSize_ + 7)), minCenterY_);
			}

			if (highlihtedParkingItems_.size()) {
				minHighlitedX_ = highlihtedParkingItems_.first()->x();
				minHighlitedY_ = highlihtedParkingItems_.first()->y();
				maxHighlitedX_ = highlihtedParkingItems_.last()->x();
				maxHighlitedY_ = highlihtedParkingItems_.last()->y();
				highligtedLenght_ = std::sqrt(std::pow(maxHighlitedX_ - minHighlitedX_, 2) +
					std::pow(maxHighlitedY_ - minHighlitedY_, 2));				
			}
		}


		bool UnitedParkingGraphicsItem::getParkingNameContains(const QString & prkName) const {
			for (auto pkrIt : unitedParkingInfo_.parkingList_) {
				if (pkrIt.name_ == prkName) {
					return true;
				}
			}
			
			return false;
		}


		void UnitedParkingGraphicsItem::updateParkingInfo(const ParkingInfo & prk) {
			for (auto prkIt : highlihtedParkingItems_) {
				if (prkIt->getParkingName() == prk.name_) {
					prkIt->updateParkingInfo(prk);
					update();
					break;
				}
			}
		}


		void UnitedParkingGraphicsItem::hoverEnterEvent(QGraphicsSceneHoverEvent* event) {
			isHighlited_ = true;
			for (auto it : highlihtedParkingItems_) {
				it->setVisible(isHighlited_);
				it->setZValue(50);
			}
			update();
		}


		void UnitedParkingGraphicsItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* event) {
			isHighlited_ = false;			
			for (auto it : highlihtedParkingItems_) {
				it->setVisible(isHighlited_);
				it->setZValue(50);
			}
			scene()->update();
		}
	}
}