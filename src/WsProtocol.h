#pragma once

#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QByteArray>
#include <QtCore/QString>

#include "SettsSingle.h"
#include "netutil.h"

namespace Fdps {
    namespace parking {

        namespace json_defs {
            const QString HeaderKey = "MessageType";

            const QString RequestParkingsHdr = "RequestParkings";

            const QString AnswerParkingsHdr = "AnswerParkings";
            const QString ParkingsKey = "Parkings";

            const QString ClientHeartbeatHdr = "ClientHeartbeat";
			const QString AppNameKey = "Name";
			const QString AppVersionKey = "Version";
			const QString AppKeyKey = "Key";
			const QString AppIPsKey = "IPs";

            const QString RequestImageHdr = "RequestImage";

            const QString AnswerImageHdr = "AnswerImage";
            const QString ImageKey = "ImageBase64";

            const QString ServerImageChangeHdr = "ServerImageChange";

            const QString ClientParkingChangeHdr = "ClientParkingChange";
			const QString ParkingKey = "Parking";

            const QString ServerParkingChangeHdr = "ServerParkingChange";

			

        }


        /// сформировать сообщение запроса стоянок
        inline QByteArray CreateRequestParkingsMsg() {
            QJsonObject jsonObj;
            jsonObj.insert(json_defs::HeaderKey, QJsonValue(json_defs::RequestParkingsHdr));
            return QJsonDocument(jsonObj).toJson();
        }

        /// сформировать сообщение запроса картинки
        inline QByteArray CreateRequestImageMsg() {
            QJsonObject jsonObj;
            jsonObj.insert(json_defs::HeaderKey, QJsonValue(json_defs::RequestImageHdr));
            return QJsonDocument(jsonObj).toJson();
        }

		/// сформировать сообщение об изменении стоянки
		inline QByteArray CreateClientParkingChangeMsg(const ParkingInfo & prk) {
			QJsonObject jsonObj;
			jsonObj.insert(json_defs::HeaderKey, QJsonValue(json_defs::ClientParkingChangeHdr));
			jsonObj.insert(json_defs::ParkingKey, prk.toJsonObject());
			return QJsonDocument(jsonObj).toJson();
		}
		
		/// сформировать сообщение heartbeat
		inline QByteArray CreateClientHeartbeatMsg() {
			QJsonObject jsonObj;
			jsonObj.insert(json_defs::HeaderKey, QJsonValue(json_defs::ClientHeartbeatHdr));
			jsonObj.insert(json_defs::AppNameKey, QJsonValue(SettsSingle::instance()->AppName()));
			jsonObj.insert(json_defs::AppVersionKey, QJsonValue(SettsSingle::instance()->AppVersion()));
			jsonObj.insert(json_defs::AppKeyKey, QJsonValue(SettsSingle::instance()->AppKey()));
			jsonObj.insert(json_defs::AppIPsKey, QJsonArray::fromStringList(Fdps::Net::Util::ip4List()));
			return QJsonDocument(jsonObj).toJson();
		}
    }
}
