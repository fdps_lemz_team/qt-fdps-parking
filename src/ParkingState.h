#pragma once

#include <QtCore/QString>
#include <QtCore/QDataStream>
#include <QtCore/QMetaType>

namespace Fdps {
	namespace parking {
		//Q_NAMESPACE

			// состояние стоянки
		enum class ParkingState {
			Occupied,
			Free,
			Locked,
			Unknown,
		};
		//Q_ENUM_NS(ParkingState)


		inline QDataStream & operator<<(QDataStream & out, const ParkingState & ps)
		{
			out << static_cast<int>(ps);
			return out;
		}

		inline QDataStream & operator>>(QDataStream & in, ParkingState & ps)
		{
			int prkSt;
			in >> prkSt;
			ps = static_cast<ParkingState>(prkSt);
			return in;
		}


		/// состояние стоянки в текстовом виде
		inline QString StateToText(const ParkingState& state) {
			switch (state) {
			case ParkingState::Locked:
				return "Закрыта";
			case ParkingState::Free:
				return "Свободна";
			case ParkingState::Occupied:
				return "Занята";
			}
			return "???";
		}

		/// состояние стоянки из текстового вида
		inline ParkingState StateFromText(const QString& stateString) {

			if (stateString == "Занята") {
				return ParkingState::Occupied;
			}
			else if (stateString == "Свободна") {
				return ParkingState::Free;
			}
			else if (stateString == "Закрыта") {
				return ParkingState::Locked;
			}
			return ParkingState::Unknown;
		}
	}
}
Q_DECLARE_METATYPE(Fdps::parking::ParkingState)