#include "WsClient.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QFile>
#include <QtCore/QTimer>
#include <QtCore/QTimerEvent>

#include "WsProtocol.h"

namespace Fdps {
	namespace parking {
		WsClient::WsClient(QObject * parent)
			: QObject(parent)
			, webSocket_(nullptr)
			, isSocketValid_(false)
			, heartbeatTimer_(this){

			connect(&heartbeatTimer_, &QTimer::timeout, this, [&] { if (isSocketValid_) { sendTextMessage(CreateClientHeartbeatMsg()); } });
			heartbeatTimer_.start(heartbeatIntervalMSecs_);
		}

		void WsClient::SetParams(const WsClientParams& params) {
			serverUrl_ = QString("ws://%1:%2/%3").arg(params.serverAddr_).arg(params.serverPort_).arg(params.serverPath_);
			connectToServer();
		}

		WsClient::~WsClient() {
			/*if (webSocket_) {
				webSocket_->disconnect();
			}*/
		}

        void WsClient::updateParams(const WsClientParams& params) {
            serverUrl_ = QString("ws://%1:%2/%3").arg(params.serverAddr_).arg(params.serverPort_).arg(params.serverPath_);
        }

		void WsClient::socketStateChanged(const QAbstractSocket::SocketState & socketState) {
			if (socketState == QAbstractSocket::SocketState::UnconnectedState) {
				destroySocket();
			}
			if (socketState == QAbstractSocket::SocketState::ConnectedState) {
				isSocketValid_ = true;
				killTimer(curCheckConnectTimerId_);
			}
			Q_EMIT stateChaned(socketState == QAbstractSocket::SocketState::ConnectedState);
		}

		void WsClient::socketDestroyed() {
			QTimer::singleShot(reconnectInterval_, this, &WsClient::connectToServer);
		}

		void WsClient::socketError(QAbstractSocket::SocketError error) {
			isSocketValid_ = false;
			QFile lastErrFile(QCoreApplication::applicationDirPath() + "/SocketErr.txt");
			if (lastErrFile.open(QIODevice::WriteOnly)) {
				lastErrFile.write(QString("Error code: %1. LastErrorString: %2").arg(error).arg(webSocket_->errorString()).toUtf8());
				lastErrFile.close();
			}

			if (error == QAbstractSocket::UnknownSocketError) {
				destroySocket();
			}
		}


		void WsClient::socketReceiveMessage(const QString& data) {
			QJsonDocument   loadDoc(QJsonDocument::fromJson(data.toUtf8()));
			const QJsonObject& json = loadDoc.object();

			if (json.contains(json_defs::HeaderKey)) {
				ParkingInfoList parkingList;

				if (json.value(json_defs::HeaderKey) == json_defs::AnswerParkingsHdr) {
					if (json.contains(json_defs::ParkingsKey) && json[json_defs::ParkingsKey].isArray()) {
						parkingList = ParkingInfoList::fromJsonArray(json[json_defs::ParkingsKey].toArray());
					}					
					if (parkingList.size()) {
						Q_EMIT parkingReceived(parkingList);
					}
				} else if (json.value(json_defs::HeaderKey) == json_defs::AnswerImageHdr) {
					if (json.contains(json_defs::ImageKey)) {
						QString imgString = json[json_defs::ImageKey].toString();
						Q_EMIT imageReceived(imgString);
					}
				} else if (json.value(json_defs::HeaderKey) == json_defs::ServerParkingChangeHdr) {
					if (json.contains(json_defs::ParkingKey)) {
						ParkingInfo prk = ParkingInfo::fromJsonObject(json[json_defs::ParkingKey].toObject());
						Q_EMIT parkingChangedReceived(prk);
					}		
				}
			}	
		}


		void WsClient::sendTextMessage(const QByteArray & dataToSend) {
			if (isSocketValid_) {
				const qint64 transmittedSize = webSocket_->sendTextMessage(dataToSend);
				Q_ASSERT(transmittedSize != -1);
			}
		}


		void WsClient::timerEvent(QTimerEvent *event) {
			Q_UNUSED(event);
			destroySocket();
		}


		void WsClient::connectToServer() {
			webSocket_ = new QWebSocket("", QWebSocketProtocol::VersionLatest, this);
			
			connect(webSocket_, &QWebSocket::stateChanged, this, &WsClient::socketStateChanged);
			
			connect(webSocket_, &QWebSocket::textMessageReceived, this, &WsClient::socketReceiveMessage);
			
			connect(webSocket_, static_cast<void (QWebSocket::*)(QAbstractSocket::SocketError)>(&QWebSocket::error),
				this, &WsClient::socketError);

			curCheckConnectTimerId_ = startTimer(waitConnectMSecs_);

			webSocket_->open(serverUrl_);
		}

		void WsClient::destroySocket() {
			if (webSocket_) {
				isSocketValid_ = false;
				killTimer(curCheckConnectTimerId_);
				webSocket_->disconnect();
				connect(webSocket_, &QWebSocket::destroyed, this, &WsClient::socketDestroyed);
				webSocket_->deleteLater();
			}
		}
	}
}