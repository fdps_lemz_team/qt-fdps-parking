#include <QtCore/qnamespace.h>
#include <QtGui/QCursor>
#include <QtGui/QPainter>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QGraphicsSceneMouseEvent>
#include <QtWidgets/QToolTip>

#include "ParkingDialog.h"
#include "ParkingGraphicsItem.h"
#include "SettsSingle.h"
#include "UtilWidgetPos.h"


namespace Fdps {
	namespace parking {

		ParkingGraphicsItem::ParkingGraphicsItem(const ParkingInfo& parkingInfo, QGraphicsItem* parent)
			: QObject()
			, QGraphicsItem(parent)
			, parkingInfo_(parkingInfo)
			, isHighlited_(false) {
			setAcceptHoverEvents(true);
			setCursor(Qt::ArrowCursor);
			setX(parkingInfo_.xPos_);
			setY(parkingInfo_.yPos_);
			updateState();
		}


		QRectF ParkingGraphicsItem::boundingRect() const {
			float curItemSize = isHighlited_ ? SettsSingle::instance()->getAppSetts().highlightItemSize_ :
				SettsSingle::instance()->getAppSetts().normalItemSize_;
			return QRectF(-curItemSize / 2, -curItemSize / 2, curItemSize, curItemSize);
		}


		void ParkingGraphicsItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
			Q_UNUSED(option);
			Q_UNUSED(widget);

			float curItemSize = isHighlited_ ? SettsSingle::instance()->getAppSetts().highlightItemSize_ :
				SettsSingle::instance()->getAppSetts().normalItemSize_;
			int curFontSize = isHighlited_ ? SettsSingle::instance()->getAppSetts().highlightFontSize_ :
				SettsSingle::instance()->getAppSetts().normalFontSize_;

			QColor curBorderColor = StateToColor(parkingInfo_.state_);

			QColor colorWithAlpha = curBorderColor;
			if (isHighlited_) {
				setZValue(30);
			} else {
				colorWithAlpha.setAlpha(100);
				setZValue(1);
			}
			painter->setBrush(QBrush(colorWithAlpha));

			painter->setPen(QPen(QBrush(curBorderColor), SettsSingle::instance()->getAppSetts().borderWidth_));
			painter->drawEllipse(-curItemSize / 2, -curItemSize / 2, curItemSize, curItemSize);
			painter->setPen(Qt::black);
			painter->setFont(QFont(SettsSingle::instance()->getAppSetts().fontName_, curFontSize, QFont::Bold, false));
			painter->drawText(-curItemSize / 2, -curItemSize / 2, curItemSize, curItemSize, Qt::AlignHCenter | Qt::AlignVCenter, parkingInfo_.name_);

		}

		QString ParkingGraphicsItem::getParkingName() const	{
			return parkingInfo_.name_;
		}

		void ParkingGraphicsItem::updateParkingInfo(const ParkingInfo & prk) {
			parkingInfo_ = prk;
			updateState();
		}


		void ParkingGraphicsItem::mousePressEvent(QGraphicsSceneMouseEvent* event) {
			ParkingDialog parkDlg(parkingInfo_);
			parkDlg.move(event->screenPos());
			qt_utils::CheckWidgetPosition(&parkDlg);
			if (parkDlg.exec() == QDialog::Accepted) {
				parkingInfo_ = parkDlg.GetParkingInfo();
				updateState();
				Q_EMIT parkingInfoChanged(parkingInfo_);
			}			
		}


		void ParkingGraphicsItem::hoverEnterEvent(QGraphicsSceneHoverEvent* event) {
			setCursor(Qt::ArrowCursor);
			isHighlited_ = true;
			update();
		}


		void ParkingGraphicsItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* event) {
			isHighlited_ = false;
			scene()->update();
			QToolTip::hideText();
		}


		void ParkingGraphicsItem::updateState() {
			setToolTip(QString("<font size=\"5\">Парковка: %1<br>"
				"Состояние: <font color=\"%2\">%3</font><br>"
				"Рейс: %4<br>"
				"Борт: %5<br>"
				"Инф.: %6<br>"
				"Разрешенные типы ВС: %7</font><br>")
				.arg(parkingInfo_.name_)
				.arg(StateToColor(parkingInfo_.state_))
				.arg(StateToText(parkingInfo_.state_))
				.arg(parkingInfo_.flightNumber_)
				.arg(parkingInfo_.sideNumber_)
				.arg(parkingInfo_.info_)
				.arg(parkingInfo_.CraftTypesString()));

			update();			
		}
	}
}