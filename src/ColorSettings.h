#pragma once

#include <QtCore/QDataStream> 
#include <QtCore/QJsonObject>
#include <QtCore/QMetaObject>
#include <QtCore/QString>

namespace Fdps {
    namespace parking {
        namespace json_defs {
            const QString colorSettsKey = "ColorSettings";
            const QString freeKey = "ColorFree";
            const QString occupiedKey = "ColorOccupied";
            const QString closedKey = "ColorClosed";
        }


        ///  цветовые настройки состояния станций.
        struct ColorSettings {
            QString freeColor;		//!< цвет подключенной станции.
            QString occupiedColor;	//!< цвет не подключенной станции
            QString closedColor;    //!< цвет остановленной станции.

            friend bool operator==(const ColorSettings& left, const ColorSettings& right) {
                return (
                    left.freeColor == right.freeColor &&
                    left.occupiedColor == right.occupiedColor &&
                    left.closedColor == right.closedColor);
            }


            friend bool operator!=(const ColorSettings& left, const ColorSettings& right) {
                return !(left == right);
            }


            /// преобразование настроек в Json объект.
            QJsonObject toJsonObject() const {
                QJsonObject retValue;
                retValue.insert(json_defs::freeKey, QJsonValue(freeColor));
                retValue.insert(json_defs::occupiedKey, QJsonValue(occupiedColor));
                retValue.insert(json_defs::closedKey, QJsonValue(closedColor));
                return retValue;
            }


            /// восстановление настроек из Json объекта.
            static ColorSettings fromJsonObject(const QJsonObject& jsonObj) {
                ColorSettings retValue;
                retValue.freeColor = jsonObj.value(json_defs::freeKey).toString("#99FF74");
                retValue.occupiedColor = jsonObj.value(json_defs::occupiedKey).toString("#E7E700");
                retValue.closedColor = jsonObj.value(json_defs::closedKey).toString("#CE0018");
                return retValue;
            }
        };


        inline QDataStream& operator<<(QDataStream& out, const ColorSettings& clSet) {
            out << clSet.freeColor << clSet.occupiedColor << clSet.closedColor;
            return out;
        }


        inline QDataStream& operator>>(QDataStream& in, ColorSettings& clSet) {
            in >> clSet.freeColor >> clSet.occupiedColor >> clSet.closedColor;
            return in;
        }
    }
}
Q_DECLARE_METATYPE(Fdps::parking::ColorSettings)
