#pragma once

#include <QtCore/QList>
#include <QtWidgets/QGraphicsItem>
#include <QtWidgets/QMenu>
#include <QtWidgets/QGraphicsScene>

#include <math.h>

#include "UnitedParkingInfo.h"
#include "ParkingGraphicsItem.h"
#include "ParkingState.h"


namespace Fdps {
	namespace parking {

		class UnitedParkingGraphicsItem : public QObject, public QGraphicsItem {
			Q_OBJECT
				Q_INTERFACES(QGraphicsItem)
		public:
			UnitedParkingGraphicsItem(const UnitedParkingInfo& unitedParkingInfo);

			QRectF boundingRect() const override;
			
			void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;
			
			/// повернуть гр. элементы стоянок
			/// \params angle - градусы
			void setParkingRotation(qreal angle);
			
			/// обновить размер обхединенной стоянки
			void updateBounds();

			/// содержится ли стоянка с такием названием в элементе
			bool getParkingNameContains(const QString &prkName) const;

			/// обновить состояние стоянки
			void updateParkingInfo(const ParkingInfo &prk);

		Q_SIGNALS:
			void parkingInfoChanged(ParkingInfo parkingInfo);

		protected:
			void hoverEnterEvent(QGraphicsSceneHoverEvent* event) override;
			void hoverLeaveEvent(QGraphicsSceneHoverEvent* event) override;


		private:			
			UnitedParkingInfo unitedParkingInfo_;
			const QColor curBorderColor_ = QColor("#C8C8C8");

			int minCenterX_, minCenterY_;
			int maxCenterX_, maxCenterY_;

			int minHighlitedX_, minHighlitedY_;
			int maxHighlitedX_, maxHighlitedY_;

			int lenght_;
			int highligtedLenght_;
			float angle_;

			QList<ParkingGraphicsItem*> highlihtedParkingItems_;

			const double pi = std::atan(1.0) * 4.0;
			const double radToDeg = 180.0 / pi;

			bool isHighlited_;

			const float highlihtedFactor_ = 2.0;
		};
	}
}
