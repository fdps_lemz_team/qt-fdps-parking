#pragma once

#include <QtWidgets/QGraphicsPixmapItem>
#include <QtWidgets/QMenu>

#include "ParkingInfo.h"
#include "ParkingState.h"


namespace Fdps {
	namespace parking {

		class ParkingGraphicsItem : public QObject, public QGraphicsItem {
			Q_OBJECT
				Q_INTERFACES(QGraphicsItem)
		public:
			ParkingGraphicsItem(const ParkingInfo& parkingInfo, QGraphicsItem* parent = nullptr);

			QRectF boundingRect() const override;

			void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

			/// �������� �������
			QString getParkingName() const;

			/// �������� ��������� �������
			void updateParkingInfo(const ParkingInfo &prk);

		Q_SIGNALS:
			void parkingInfoChanged(ParkingInfo parkingInfo);

		protected:
			void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
			void hoverEnterEvent(QGraphicsSceneHoverEvent* event) override;
			void hoverLeaveEvent(QGraphicsSceneHoverEvent* event) override;

		private:
			void updateState();

			ParkingInfo parkingInfo_;
			bool isHighlited_;
		};
	}
}
