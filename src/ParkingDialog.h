#pragma  once

#include <QtWidgets/QDialog>

#include "ParkingInfo.h"

#include "ui_ParkingDialog.h"

namespace Fdps {
	namespace parking {
		/// окно оредактирования сведений о стоянке
		class ParkingDialog : public QDialog {
			Q_OBJECT

		public:
			ParkingDialog(const ParkingInfo& parkingInfo, QWidget* parent = 0);

			/// предоставить измененную информацию о стоянке
			ParkingInfo GetParkingInfo();

		/*Q_SIGNALS:
			/// изменено состояние стоянки
			void stateChanged(ParkingState state);*/

		private:
			/// инициализация виджета состоянием стоянки
			void InitByParkingInfo();

			void ClearParkingInfo();
			
			void RememberParkingInfo();

			Ui::ParkingDialog gui_;

			ParkingInfo parkingInfo_;
		};
	}
}
