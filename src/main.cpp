#include "MainWindow.h"

#include <QtSingleApplication>

#if QT_VERSION < 0x050000
	#include <QTextCodec>
#endif


int main(int argc, char **argv)
{
#if QT_VERSION < 0x050000
	QTextCodec::setCodecForTr( QTextCodec::codecForName( "UTF-8" ) );
#endif

    QtSingleApplication singleApplication("FdpsParking", argc, argv);

    singleApplication.setOrganizationName("ALMAZ");
    singleApplication.setApplicationName("FdpsParking");

    if (singleApplication.sendMessage("Wake up!"))
    {
        return 0;
    }

    singleApplication.initialize();

    Fdps::parking::MainWindow mainWidget;
    mainWidget.show();

    singleApplication.setActivationWindow(&mainWidget);
    return singleApplication.exec();	
}
