#pragma  once

#include <QtCore/QList>
#include <QtCore/QDateTime>
#include <QtCore/QSortFilterProxyModel>
#include <QtCore/QTimer>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QtWidgets/QMainWindow>

#include "WsClient.h"
#include "ParkingTableModel.h"

#include "ui_MainWindow.h"

namespace Fdps {
	namespace parking {

		class ParkingGraphicsItem;
		class UnitedParkingGraphicsItem;

		class MainWindow : public QMainWindow {
			Q_OBJECT

		public:
			MainWindow(QWidget* parent = 0);
			~MainWindow();

		public Q_SLOTS:		
			/// получены сведения о стоянках
			void parkingReceived(const ParkingInfoList& pl);
			/// получена картинка
			void imageReceived(const QString& img);

		private Q_SLOTS:
			/// применение настроек
			void applyBtnClicked();
			/// нажатие на кнопку с цветом.
			void colorButtonClicked();
			/// изменен шрифт
			void fontFamilyTextChanged(const QString& text);
			/// нажатие на кнопку увеличить масштаб.
			void zoomInButtonClicked();
			/// нажатие на кнопку уменьшить масштаб.
			void zoomOutButtonClicked();
			/// обновлены сведения о стоянке (из GUI)
			void parkingUpdated(const ParkingInfo &prk);
			/// обновлены сведения о стоянке ( от сервиса )
			void parkingChangedReceived(const ParkingInfo &prk);
			/// изменено состояние подключения WS клиента
			void wsClientConnStateChanged(const bool &isConn);
			/// клик по таблице стоянок
			void parkingTableDoubleClicked(const QPoint& itemPos, const QPoint& screenPos);

		Q_SIGNALS:
			void wsSettingsUpdated(const WsClientParams&);
			/// отправить данные.
			void wsSendTextMessage(const QByteArray& dataToSend);

		private:
			/// инициализация вкладки "Настройки"
			void initSettingsTab();
			/// отрисовка стоянок
			void drawParkings(const ParkingInfoList& prkList);
			/// задать цвет кнопке.
			void setColorToButton(QPushButton* buttonPtr, const QString& colorName);
			/// получить цвет кнопки.
			QString getColorName(QPushButton* buttonPtr) const;
			/// обновить доступность кнопок "Применить" и "Отмена"
			void settingsChanged();
			/// задание матрицы для масштабирования
			void setupMatrix();			
			/// сбросить параметры сцены
			void resetScene();

			Ui::MainWindow gui_;
			parking::WsClient wsClient_;
			QGraphicsScene* scene_;
			QList<ParkingGraphicsItem*> parkGraphicsItems_;
			QList<UnitedParkingGraphicsItem*> unitedParkGraphicsItems_;
			QPixmap appronPixmap_;	//!< картинка перрона, получаемая из сервиса go
			QIcon mainWindowIcon_;	//!< иконка главного окна.
			QSortFilterProxyModel prkProxyModel_;
			ParkingTableModel prkSourceModel_;
		};
	}
}
