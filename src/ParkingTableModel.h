#pragma once 

#include <QAbstractTableModel>

#include "ParkingInfo.h"


namespace Fdps {
	namespace parking {

		/// Модель отображения списка РЛС для окна настроек.
		class ParkingTableModel : public QAbstractTableModel {
			Q_OBJECT
		public:
			ParkingTableModel(QObject* parent);

			virtual int columnCount(const QModelIndex&) const;

			virtual int rowCount(const QModelIndex&) const;

			virtual QVariant data(const QModelIndex& index, int role) const;

			virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

			void setPrkList(const ParkingInfoList &prkList);

			void updatePrk(const ParkingInfo &prk);

			ParkingInfo getPrkByIndex(const QModelIndex& index) const;

		private:
			ParkingInfoList prkList_;
		};
	}
}