#pragma once

#include <QtWidgets/QTableView>
#include <QtGui/QKeyEvent>

class TableViewWithClick : public QTableView
{
    Q_OBJECT
public:
    TableViewWithClick(QWidget * parent = 0)
        : QTableView(parent)
    {
    }

protected:
    virtual void mouseDoubleClickEvent(QMouseEvent* event)
    {
        Q_EMIT mouseDoubleClicked(event->pos(), event->screenPos().toPoint());
        
    }

Q_SIGNALS:
    void mouseDoubleClicked(QPoint itemPos, QPoint screenPos);
};
