#include "ParkingTableModel.h"

#include <QtGui/QColor>

#include "SettsSingle.h"

namespace Fdps {
	namespace parking {

		ParkingTableModel::ParkingTableModel(QObject* parent)
			: QAbstractTableModel(parent){
		}


		int ParkingTableModel::columnCount(const QModelIndex&) const {
			return 6;
		}


		int ParkingTableModel::rowCount(const QModelIndex&) const {
			return prkList_.size();
		}


		QVariant ParkingTableModel::data(const QModelIndex& index, int role) const {
			if (index.row() < prkList_.size()) {
				const auto curPrk = prkList_.at(index.row());

				if (role == Qt::DisplayRole) {
					switch (index.column())
					{
					case 0:
						return curPrk.name_;
					case 1:
						return StateToText(curPrk.state_);
					case 2:
						return curPrk.flightNumber_;
					case 3:
						return curPrk.sideNumber_;
					case 4:
						return curPrk.info_;
					case 5:
						return curPrk.vsTypes_;
					default:
						return QVariant();
					}
				}

				if (role == Qt::BackgroundRole)	{
					QColor curColor = QColor(StateToColor(curPrk.state_));
					curColor.setAlpha(60);
					return curColor;
				}
			}


			return QVariant();
		}


		QVariant ParkingTableModel::headerData(int section, Qt::Orientation orientation, int role) const {
			if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
				switch (section) {
				default:
					return tr("");
				case 0:
					return tr("Название");
				case 1:
					return tr("Состояние");
				case 2:
					return tr("Номер рейса");
				case 3:					
					return tr("Бортовой номер");
				case 4:					
					return tr("Примечание");
				case 5:					
					return tr("Типы принимаемых ВС");
				}
			}
			return QVariant();
		}


		void ParkingTableModel::setPrkList(const ParkingInfoList& prkList) {
			beginResetModel();
			prkList_ = prkList;
			endResetModel();
		}


		void ParkingTableModel::updatePrk(const ParkingInfo& prk) {
			for (auto nn = 0; nn < prkList_.size(); ++nn)
			{
				if (prkList_.at(nn).name_ == prk.name_)
				{
					prkList_[nn] = prk;
					Q_EMIT dataChanged(createIndex(nn, 0),
						createIndex(nn, columnCount(createIndex(0, 0)) - 1), { Qt::DisplayRole });
				}
			}

		}


		ParkingInfo ParkingTableModel::getPrkByIndex(const QModelIndex& index) const
		{
			if (index.row() < prkList_.size()) {
				return prkList_.at(index.row());
			}
			return ParkingInfo();
		}
	}
}