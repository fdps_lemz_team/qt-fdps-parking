#pragma once

#include <QtCore/QDataStream> 
#include <QtCore/QJsonObject>
#include <QtCore/QMetaObject>
#include <QtCore/QString>

#include "ColorSettings.h"
#include "WsClientSettings.h"

namespace Fdps {
    namespace parking {
        namespace json_defs {
            const QString rotateAngleKey = "RotateAngle";
            const QString borderWidthKey = "BorderWidth";
            const QString normalItemSizeKey = "NormalItemSize";
            const QString highlightItemSizeKey = "HighlightItemSize";
            const QString fontNameKey = "FontName";
            const QString normalFontSizeKey = "NormalFontSize";
            const QString highlightFontSizeKey = "HighlightFontSize";
            const QString scaleKey = "Scale";
        }


        ///  цветовые настройки состояния станций.
        struct AppSettings {
            int rotateAngle_;           //!< угол поворота картинки и элементов.
            int borderWidth_;           //!< толщина рамки элемента стоянки
            float normalItemSize_;      //!< размер элемента стоянки (px)
            float highlightItemSize_;   //!< размер элемента стоянки в выделенном состоянии (px)
            QString fontName_;          //!< название шрифта элемента стоянки
            int normalFontSize_;        //!< размер шрифта элемента стоянки
            int highlightFontSize_;     //!< размер шрифта элемента стоянки в выделенном состоянии
            ColorSettings colorSetts_;  //!< цветовые настройки стоянов
            WsClientParams wsSetts_;    //!< параметры подключения к сервису go
            qreal scale_;               //!< условный масштаб [1, 10]

            friend bool operator==(const AppSettings& left, const AppSettings& right) {
                return (
                    left.rotateAngle_ == right.rotateAngle_ &&
                    left.borderWidth_ == right.borderWidth_ &&
                    left.normalItemSize_ == right.normalItemSize_ &&
                    left.highlightItemSize_ == right.highlightItemSize_ &&
                    left.fontName_ == right.fontName_ &&
                    left.normalFontSize_ == right.normalFontSize_ &&
                    left.highlightFontSize_ == right.highlightFontSize_ &&
                    left.colorSetts_ == right.colorSetts_ &&
                    left.wsSetts_ == right.wsSetts_ &&
                    left.scale_ == right.scale_);
            }


            friend bool operator!=(const AppSettings& left, const AppSettings& right) {
                return !(left == right);
            }


            /// преобразование настроек в Json объект.
            QJsonObject toJsonObject() const {
                QJsonObject retValue;
                retValue.insert(json_defs::rotateAngleKey, QJsonValue(rotateAngle_));
                retValue.insert(json_defs::borderWidthKey, QJsonValue(borderWidth_));
                retValue.insert(json_defs::normalItemSizeKey, QJsonValue(normalItemSize_));
                retValue.insert(json_defs::highlightItemSizeKey, QJsonValue(highlightItemSize_));
                retValue.insert(json_defs::fontNameKey, QJsonValue(fontName_));
                retValue.insert(json_defs::normalFontSizeKey, QJsonValue(normalFontSize_));
                retValue.insert(json_defs::highlightFontSizeKey, QJsonValue(highlightFontSize_));
                retValue.insert(json_defs::colorSettsKey, QJsonValue(colorSetts_.toJsonObject()));
                retValue.insert(json_defs::wsSettsKey, QJsonValue(wsSetts_.toJsonObject()));
                retValue.insert(json_defs::scaleKey, QJsonValue(scale_));
                return retValue;
            }


            /// восстановление настроек из Json объекта.
            static AppSettings fromJsonObject(const QJsonObject& jsonObj) {
                AppSettings retValue;
                retValue.rotateAngle_ = jsonObj.value(json_defs::rotateAngleKey).toInt(0);
                retValue.borderWidth_ = jsonObj.value(json_defs::borderWidthKey).toInt(3);
                retValue.normalItemSize_ = jsonObj.value(json_defs::normalItemSizeKey).toDouble(20.0);
                retValue.highlightItemSize_ = jsonObj.value(json_defs::highlightItemSizeKey).toDouble(40.0);
                retValue.fontName_ = jsonObj.value(json_defs::fontNameKey).toString("Courier New");
                retValue.normalFontSize_ = jsonObj.value(json_defs::normalFontSizeKey).toInt(8);
                retValue.highlightFontSize_ = jsonObj.value(json_defs::highlightFontSizeKey).toInt(12);
                retValue.colorSetts_ = ColorSettings::fromJsonObject(jsonObj.value(json_defs::colorSettsKey).toObject(QJsonObject()));
                retValue.wsSetts_ = WsClientParams::fromJsonObject(jsonObj.value(json_defs::wsSettsKey).toObject(QJsonObject()));
                retValue.scale_ = jsonObj.value(json_defs::scaleKey).toDouble(1.0);
                return retValue;
            }
        };
    }
}
Q_DECLARE_METATYPE(Fdps::parking::AppSettings)
