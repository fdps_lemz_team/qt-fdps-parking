#pragma once

#include <QtCore/QFile>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QMetaType> 
#include <QtCore/QString>
#include <QtCore/QStringList>

#include "ParkingState.h"

namespace Fdps {
    namespace parking {
        const QString PrkKey = "Parkings";
		const QString PrkAirportKey = "Airport";
        const QString PrkNameKey = "Name";
        const QString PrkUnitedNameKey = "UnitedName";
        const QString PrkCraftKey = "CraftTypes";
        const QString PrkStateKey = "State";
        const QString PrkFlightKey = "Flight";
        const QString PrkSideKey = "Side";
        const QString PrkInfoKey = "Info";
        const QString PrkXKey = "X";
        const QString PrkYKey = "Y";


        /// информация о стоянке
        struct ParkingInfo {
            QString name_;				//!< название стоянки
            QString unitedName_;	    //!< название объединяющей стоянки
			QString icaoCode_;			//!< ICAO код аэродрома
            QStringList vsTypes_;		//!< типы принимаемых ВС
            ParkingState state_;		//!< состояние стоянки
            QString flightNumber_;		//!< номер рейса
            QString sideNumber_;		//!< бортовой номер
            QString info_;				//!< примечание
            int xPos_;					//!< координата X на сцене
            int yPos_;					//!< координата Y на сцене

            /// строка с типами принимаемых ВС
            QString CraftTypesString() const {
                QString retValue;
                for (auto it : vsTypes_) {
                    retValue += it + "\n";
                }
                return retValue;
            }


            friend bool operator==(const ParkingInfo& left, const ParkingInfo& right) {
                return (left.name_ == right.name_ &&
                    left.unitedName_ == right.unitedName_ &&
					left.icaoCode_ == right.icaoCode_ &&
                    left.vsTypes_ == right.vsTypes_ &&
                    left.state_ == right.state_ &&
                    left.flightNumber_ == right.flightNumber_ &&
                    left.sideNumber_ == right.sideNumber_ &&
                    left.info_ == right.info_ &&
                    left.yPos_ == right.xPos_ &&
                    left.yPos_ == right.yPos_
                    );
            }


            friend bool operator!=(const ParkingInfo& left, const ParkingInfo& right) {
                return !(left == right);
            }

            /// преобразование стоянки в Json объект.
            QJsonObject toJsonObject() const {
                QJsonObject retValue;
                retValue.insert(PrkNameKey, name_);
                retValue.insert(PrkUnitedNameKey, unitedName_);
				retValue.insert(PrkAirportKey, icaoCode_);

                QJsonArray craftArray;
                for (auto it : vsTypes_) {
                    craftArray.append(it);
                }
                retValue.insert(PrkCraftKey, craftArray);

                retValue.insert(PrkStateKey, StateToText(state_));
                retValue.insert(PrkFlightKey, flightNumber_);
                retValue.insert(PrkSideKey, sideNumber_);
                retValue.insert(PrkInfoKey, info_);
                retValue.insert(PrkXKey, xPos_);
                retValue.insert(PrkYKey, yPos_);

                return retValue;
            }


            /// стоянка из Json объекта.        
            static ParkingInfo fromJsonObject(const QJsonObject& jsonObj) {
                ParkingInfo retValue;
                retValue.name_ = jsonObj.value(PrkNameKey).toString();
                retValue.unitedName_ = jsonObj.value(PrkUnitedNameKey).toString();
				retValue.icaoCode_= jsonObj.value(PrkAirportKey).toString();

                const QJsonValue craftArrayValue = jsonObj.value(PrkCraftKey);
                if (craftArrayValue.isArray()) {
                    const QJsonArray craftArray = craftArrayValue.toArray();

                    for (auto it : craftArray) {
                        if (it.isString()) {
                            retValue.vsTypes_.append(it.toString());
                        }
                    }
                }

                retValue.state_ = StateFromText(jsonObj.value(PrkStateKey).toString());
                retValue.flightNumber_ = jsonObj.value(PrkFlightKey).toString();
                retValue.sideNumber_ = jsonObj.value(PrkSideKey).toString();
                retValue.info_ = jsonObj.value(PrkInfoKey).toString();
                retValue.xPos_ = jsonObj.value(PrkXKey).toInt();
                retValue.yPos_ = jsonObj.value(PrkYKey).toInt();

                return retValue;
            }
        };

		inline QDataStream & operator<<(QDataStream & out, const ParkingInfo & pi)
		{
			out << pi.name_ << pi.unitedName_ << pi.icaoCode_ << pi.vsTypes_ << static_cast<int>(pi.state_)
				<< pi.flightNumber_	<< pi.sideNumber_ << pi.info_ << pi.xPos_ << pi.yPos_;
			return out;
		}

		inline QDataStream & operator>>(QDataStream & in, ParkingInfo & pi)
		{
			int prkSt;
			in >> pi.name_ >> pi.unitedName_  >> pi.icaoCode_ >> pi.vsTypes_ >> prkSt >> pi.flightNumber_
				>> pi.sideNumber_ >> pi.info_ >> pi.xPos_ >> pi.yPos_;
			pi.state_ = static_cast<ParkingState>(prkSt);
			return in;
		}

        struct ParkingInfoList : public QList<ParkingInfo> {
            
            /// список стоянок из Json массива.        
            static ParkingInfoList fromJsonArray(const QJsonArray& jsonArr) {
                ParkingInfoList retValue;
                for (auto jsonObgIt : jsonArr) {
                    retValue.append(ParkingInfo::fromJsonObject(jsonObgIt.toObject()));
                }
                return retValue;
            }
        };         
    }
}
Q_DECLARE_METATYPE(Fdps::parking::ParkingInfo)
Q_DECLARE_METATYPE(Fdps::parking::ParkingInfoList)