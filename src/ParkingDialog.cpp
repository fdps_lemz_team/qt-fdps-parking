#include "ParkingDialog.h"

#include "SettsSingle.h"

namespace Fdps {
	namespace parking {

		ParkingDialog::ParkingDialog(const ParkingInfo& parkingInfo, QWidget* parent /*= 0 */)
			: QDialog(parent)
			, parkingInfo_(parkingInfo) {
			gui_.setupUi(this);
			//setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);	
			setWindowFlags(windowFlags() | Qt::FramelessWindowHint);
			gui_.lbl_ParkingName->setText(parkingInfo_.name_);

			InitByParkingInfo();

			connect(gui_.btn_Occupy, &QPushButton::clicked, this, [&]() {
				parkingInfo_.state_ = ParkingState::Occupied;
				RememberParkingInfo();
				InitByParkingInfo();
				});

			connect(gui_.btn_Free, &QPushButton::clicked, this, [&]() {
				parkingInfo_.state_ = ParkingState::Free;
				ClearParkingInfo();
				InitByParkingInfo();
				});

			connect(gui_.btn_Lock, &QPushButton::clicked, this, [&]() {
				parkingInfo_.state_ = ParkingState::Locked;
				ClearParkingInfo();
				InitByParkingInfo();
				});

			connect(gui_.btn_Unlock, &QPushButton::clicked, this, [&]() {
				parkingInfo_.state_ = ParkingState::Free;
				ClearParkingInfo();
				InitByParkingInfo();
				});

			connect(gui_.btn_Save, &QPushButton::clicked, this, [&]() { QDialog::accept(); });

			connect(gui_.btn_Cancel, &QPushButton::clicked, this, [&]() { QDialog::reject(); });
		}


		ParkingInfo ParkingDialog::GetParkingInfo() {
			parkingInfo_.flightNumber_ = gui_.cbx_FlightNumber->currentText();
			parkingInfo_.sideNumber_ = gui_.le_SideNumber->text();
			parkingInfo_.info_ = gui_.txt_Info->toPlainText();

			return parkingInfo_;
		}


		void ParkingDialog::InitByParkingInfo() {
			QPalette palette = gui_.lbl_ParkingName->palette();
			palette.setColor(gui_.lbl_ParkingName->backgroundRole(), QColor(StateToColor(parkingInfo_.state_)));
			gui_.lbl_ParkingName->setPalette(palette);
			
			gui_.cbx_FlightNumber->setEditText(parkingInfo_.flightNumber_);
			gui_.le_SideNumber->setText(parkingInfo_.sideNumber_);
			gui_.txt_Info->setText(parkingInfo_.info_);
			gui_.btn_Free->setEnabled(parkingInfo_.state_ == ParkingState::Occupied);
			gui_.btn_Occupy->setEnabled(parkingInfo_.state_ == ParkingState::Free);
			gui_.btn_Lock->setEnabled(parkingInfo_.state_ != ParkingState::Locked);
			gui_.btn_Unlock->setEnabled(parkingInfo_.state_ == ParkingState::Locked);
		}


		void ParkingDialog::ClearParkingInfo() {
			parkingInfo_.flightNumber_ = "";
			parkingInfo_.sideNumber_ = "";
			parkingInfo_.info_ = "";
		}


		void ParkingDialog::RememberParkingInfo() {
			parkingInfo_.flightNumber_ = gui_.cbx_FlightNumber->currentText();
			parkingInfo_.sideNumber_ = gui_.le_SideNumber->text();
			parkingInfo_.info_ = gui_.txt_Info->toPlainText();
		}
	}
}