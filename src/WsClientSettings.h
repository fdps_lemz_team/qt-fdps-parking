#pragma once

#include <QtCore/QDataStream> 
#include <QtCore/QJsonObject>
#include <QtCore/QMetaObject>
#include <QtCore/QString>


namespace Fdps {
    namespace parking {

        namespace json_defs {
            const QString wsSettsKey = "WsParams";
            const QString addrKey = "Addr";
            const QString portKey = "Port";
            const QString pathKey = "Host";
        }

        /// настройки клиента
        struct WsClientParams {
            QString serverAddr_;
            int	serverPort_;
            QString serverPath_;

            friend bool operator==(const WsClientParams& left, const WsClientParams& right) {
                return (
                    left.serverAddr_ == right.serverAddr_ &&
                    left.serverPort_ == right.serverPort_ &&
                    left.serverPath_ == right.serverPath_);
            }


            friend bool operator!=(const WsClientParams& left, const WsClientParams& right) {
                return !(left == right);
            }


            /// преобразование настроек в Json объект.
            QJsonObject toJsonObject() const {
                QJsonObject retValue;
                retValue.insert(json_defs::addrKey, QJsonValue(serverAddr_));
                retValue.insert(json_defs::portKey, QJsonValue(serverPort_));
                retValue.insert(json_defs::pathKey, QJsonValue(serverPath_));
                return retValue;
            }


            /// восстановление настроек из Json объекта.
            static WsClientParams fromJsonObject(const QJsonObject& jsonObj) {
                WsClientParams retValue;
                retValue.serverAddr_ = jsonObj.value(json_defs::addrKey).toString("127.0.0.1");
                retValue.serverPort_ = jsonObj.value(json_defs::portKey).toInt(8072);
                retValue.serverPath_ = jsonObj.value(json_defs::pathKey).toString("parking");
                return retValue;
            }
        };

        inline QDataStream& operator<<(QDataStream& out, const WsClientParams& wsSet) {
            out << wsSet.serverAddr_ << wsSet.serverPort_ << wsSet.serverPath_;
            return out;
        }


        inline QDataStream& operator>>(QDataStream& in, WsClientParams& wsSet) {
            in >> wsSet.serverAddr_ >> wsSet.serverPort_ >> wsSet.serverPath_;
            return in;
        }
    }
}
Q_DECLARE_METATYPE(Fdps::parking::WsClientParams)
