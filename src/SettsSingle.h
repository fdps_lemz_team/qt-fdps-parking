#pragma once

#include <QtCore/QCoreApplication>
#include <QtCore/QString>
#include <QtCore/QUuid>

#include "AppSettings.h"
#include "ColorSettings.h"
#include "ParkingInfo.h"
#include "UnitedParkingInfo.h"

namespace Fdps {
    namespace parking {



        /// класс для хранения и предоставления настроек приложения
        class SettsSingle : public QObject {
            Q_OBJECT

        private:
            SettsSingle()
                : settingsFilePath_(QCoreApplication::applicationDirPath() + "/settings.json")
				, appInstanceUniqueKey_(QUuid::createUuid().toString())
				, appInstanceVersion_("2020.05.07")
				, appInstanceName_("FDPS-PARKINGS"){

                readSettingsFromFile();
            }

            void readSettingsFromFile() {
                QFile file(settingsFilePath_);
                if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
                    // настройки по умолчанию
                    appSettings_ = AppSettings::fromJsonObject(QJsonObject());
                    writeSettingsToFile();
                    return;
                }
                QJsonDocument jsonDoc = QJsonDocument::fromJson(file.readAll());
                file.close();

                appSettings_ = AppSettings::fromJsonObject(jsonDoc.object());

                if (!jsonDoc.isObject()) {
                    writeSettingsToFile();
                }
            }

            void writeSettingsToFile() {
                QFile file(settingsFilePath_);
                if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
                    return;
                }

                file.write(QJsonDocument(appSettings_.toJsonObject()).toJson());
                file.close();
            }

            static SettsSingle* instance_;
            AppSettings appSettings_;
            ColorSettings colorSettings_;
            const QString settingsFilePath_;
			const QString appInstanceUniqueKey_;
			const QString appInstanceVersion_;
			const QString appInstanceName_;

        public:
            static SettsSingle* instance() {
                if (!instance_)
                    instance_ = new SettsSingle();
                return instance_;
            }

            AppSettings getAppSetts() const {
                return appSettings_;
            }

            void setAppSetts(const AppSettings& appSetts) {
                appSettings_ = appSetts;
                writeSettingsToFile();
            }

            bool updateScale(const qreal& scaleDiff) {
                if (appSettings_.scale_ + scaleDiff >= 1.0 && appSettings_.scale_ + scaleDiff <= 7.0) {
                    appSettings_.scale_ += scaleDiff;
                    writeSettingsToFile();
                    return true;
                }
                return false;
            }

			QString AppName() const {
				return appInstanceName_;
			}

			QString AppKey() const {
				return appInstanceUniqueKey_;
			}

			QString AppVersion() const {
				return appInstanceVersion_;
			}

        };

        /// цвет стоянки
        inline QString StateToColor(const ParkingState& state) {
            switch (state) {
            case ParkingState::Locked:
                return SettsSingle::instance()->getAppSetts().colorSetts_.closedColor;
            case ParkingState::Free:
                return SettsSingle::instance()->getAppSetts().colorSetts_.freeColor;
            case ParkingState::Occupied:
                return SettsSingle::instance()->getAppSetts().colorSetts_.occupiedColor;
            }
            return "#00000";
        }	
    }
}
