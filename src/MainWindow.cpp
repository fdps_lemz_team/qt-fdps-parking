#include "MainWindow.h"

#include <QtWidgets/QColorDialog>
#include <QtGui/QFontDatabase> 
#include <QtGui/QPen>
#include <qmath.h>

#include "ParkingDialog.h"
#include "ParkingGraphicsItem.h"
#include "SettsSingle.h"
#include "UnatedParkingGraphicsItem.h"
#include "UtilWidgetParams.h"
#include "UtilWidgetPos.h"
#include "WsProtocol.h"

namespace qt_utils {
	QString WidgetParams::filePath_ = "";	
}

namespace Fdps {
	namespace parking {

		SettsSingle* SettsSingle::instance_ = nullptr;

		MainWindow::MainWindow(QWidget* parent /*= 0 */)
			: QMainWindow(parent)
			, scene_(new QGraphicsScene())
			, prkProxyModel_(this)
			, prkSourceModel_(this){
			qRegisterMetaType<Fdps::parking::ParkingState>("Fdps::parking::ParkingState");
			qRegisterMetaTypeStreamOperators<Fdps::parking::ParkingState>("Fdps::parking::ParkingState");

			qRegisterMetaType<Fdps::parking::ParkingInfo>("Fdps::parking::ParkingInfo");
			qRegisterMetaTypeStreamOperators<Fdps::parking::ParkingInfo>("Fdps::parking::ParkingInfo");
			qRegisterMetaType<Fdps::parking::ParkingInfoList>("Fdps::parking::ParkingInfoList");
			qRegisterMetaTypeStreamOperators<Fdps::parking::ParkingInfoList>("Fdps::parking::ParkingInfoList");

			qRegisterMetaType<Fdps::parking::UnitedParkingInfo>("Fdps::parking::UnitedParkingInfo");
			qRegisterMetaTypeStreamOperators<Fdps::parking::UnitedParkingInfo>("Fdps::parking::UnitedParkingInfo");
			qRegisterMetaType<Fdps::parking::UnitedParkingInfoList>("Fdps::parking::UnitedParkingInfoList");
			qRegisterMetaTypeStreamOperators<Fdps::parking::UnitedParkingInfoList>("Fdps::parking::UnitedParkingInfoList");

			gui_.setupUi(this);
			setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

			mainWindowIcon_.addFile(QStringLiteral(":/icons/parkings.png"), QSize(), QIcon::Normal, QIcon::Off);
			setWindowIcon(mainWindowIcon_);

			// Cбор и фильтрация шрифтов
			QFontDatabase fontFamilies;
			QStringList fontList(fontFamilies.families(QFontDatabase::Cyrillic));
			for (const QString family : fontList) {
				QFontDatabase fontProperty;
				if (!fontProperty.isScalable(family)) {
					fontList.removeAt(fontList.indexOf(family));
				}
			}
			gui_.cb_FontFamily->addItems(fontList);

			connect(gui_.tbtn_ZoomIn, &QPushButton::clicked, this, &MainWindow::zoomInButtonClicked);

			connect(gui_.tbtn_ZoomOut, &QPushButton::clicked, this, &MainWindow::zoomOutButtonClicked);

			connect(gui_.btn_Apply, &QPushButton::clicked, this, &MainWindow::applyBtnClicked);

			connect(gui_.btn_Cancel, &QPushButton::clicked, this, &MainWindow::initSettingsTab);

			connect(gui_.cb_FontFamily, &QComboBox::currentTextChanged, this, &MainWindow::fontFamilyTextChanged);

			connect(gui_.btn_Free, &QPushButton::clicked, this, &MainWindow::colorButtonClicked);
			connect(gui_.btn_Occupied, &QPushButton::clicked, this, &MainWindow::colorButtonClicked);
			connect(gui_.btn_Closed, &QPushButton::clicked, this, &MainWindow::colorButtonClicked);

			connect(gui_.spb_ParkingSizeNormal, QOverload<int>::of(&QSpinBox::valueChanged), this, &MainWindow::settingsChanged);
			connect(gui_.spb_ParkingSizeHighlited, QOverload<int>::of(&QSpinBox::valueChanged), this, &MainWindow::settingsChanged);
			connect(gui_.spb_ParkingBorderWidth, QOverload<int>::of(&QSpinBox::valueChanged), this, &MainWindow::settingsChanged);
			connect(gui_.spb_RotateAngle, QOverload<int>::of(&QSpinBox::valueChanged), this, &MainWindow::settingsChanged);
			connect(gui_.cb_FontFamily, &QComboBox::currentTextChanged, this, &MainWindow::settingsChanged);
			connect(gui_.spb_FontSizeNormal, QOverload<int>::of(&QSpinBox::valueChanged), this, &MainWindow::settingsChanged);
			connect(gui_.spb_FontSizeHighlited, QOverload<int>::of(&QSpinBox::valueChanged), this, &MainWindow::settingsChanged);

			connect(gui_.le_Address, &QLineEdit::textChanged, this, &MainWindow::settingsChanged);
			connect(gui_.spb_Port, QOverload<int>::of(&QSpinBox::valueChanged), this, &MainWindow::settingsChanged);
			connect(gui_.le_Path, &QLineEdit::textChanged, this, &MainWindow::settingsChanged);

			connect(gui_.tblv_Parkings, &TableViewWithClick::mouseDoubleClicked, this, &MainWindow::parkingTableDoubleClicked);

			prkProxyModel_.setSourceModel(&prkSourceModel_);

			gui_.tblv_Parkings->setSortingEnabled(true);
			gui_.tblv_Parkings->verticalHeader()->setDefaultSectionSize(20);
			gui_.tblv_Parkings->verticalHeader()->setVisible(false);
			gui_.tblv_Parkings->horizontalHeader()->setDefaultSectionSize(150);
			gui_.tblv_Parkings->horizontalHeader()->setDefaultAlignment(Qt::AlignHCenter);
			gui_.tblv_Parkings->horizontalHeader()->setStretchLastSection(true);
			gui_.tblv_Parkings->setSelectionBehavior(QAbstractItemView::SelectRows);
			gui_.tblv_Parkings->setSelectionMode(QAbstractItemView::SingleSelection);
			gui_.tblv_Parkings->setModel(&prkProxyModel_);

			//connect(&wsClient_, &WsClient::receiveMessage, this, &MainWindow::wsReceived);
			connect(&wsClient_, &WsClient::stateChaned, this, &MainWindow::wsClientConnStateChanged);
			//connect(&wsClient_, &WsClient::needUpdateSettings, this, &MainWindow::refreshWsSettings);
			connect(this, &MainWindow::wsSettingsUpdated, &wsClient_, &WsClient::updateParams);
			connect(&wsClient_, &WsClient::parkingReceived, this, &MainWindow::parkingReceived);
			connect(&wsClient_, &WsClient::imageReceived, this, &MainWindow::imageReceived);
			connect(&wsClient_, &WsClient::parkingChangedReceived, this, &MainWindow::parkingChangedReceived);
			connect(this, &MainWindow::wsSendTextMessage, &wsClient_, &WsClient::sendTextMessage);

			wsClient_.SetParams(SettsSingle::instance()->getAppSetts().wsSetts_);

			gui_.grv_Parking->setScene(scene_);

			initSettingsTab();

			gui_.grv_Parking->setCursor(Qt::ArrowCursor);
			gui_.grv_Parking->setRenderHint(QPainter::Antialiasing);
			gui_.grv_Parking->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
			gui_.grv_Parking->setBackgroundBrush(QColor("#F0F0F0"));

			setCursor(Qt::ArrowCursor);

			setupMatrix();

			qt_utils::WidgetParams::restore(this);
		}


		MainWindow::~MainWindow() {
			qt_utils::WidgetParams::save(this);
		}


		void MainWindow::parkingReceived(const ParkingInfoList& pl) {
			prkSourceModel_.setPrkList(pl);
			drawParkings(pl);
		}


		void MainWindow::imageReceived(const QString& img) {
			QByteArray by = QByteArray::fromBase64(img.toUtf8());
			appronPixmap_.loadFromData(by, "JPEG");			

			Q_EMIT wsSendTextMessage(CreateRequestParkingsMsg());	

			resetScene();
		}


		void MainWindow::applyBtnClicked() {
			// поворачиваем до 0 (до задания новых настроек)
			for (auto pIt : parkGraphicsItems_) {
				pIt->setRotation(SettsSingle::instance()->getAppSetts().rotateAngle_);
			}
			for (auto upIt : unitedParkGraphicsItems_) {
				upIt->setParkingRotation(SettsSingle::instance()->getAppSetts().rotateAngle_);
			}

			AppSettings curAppSetts;
			curAppSetts.rotateAngle_ = gui_.spb_RotateAngle->value();
			curAppSetts.borderWidth_ = gui_.spb_ParkingBorderWidth->value();
			curAppSetts.normalItemSize_ = gui_.spb_ParkingSizeNormal->value();
			curAppSetts.highlightItemSize_ = gui_.spb_ParkingSizeHighlited->value();
			curAppSetts.fontName_ = gui_.cb_FontFamily->currentText();
			curAppSetts.normalFontSize_ = gui_.spb_FontSizeNormal->value();
			curAppSetts.highlightFontSize_ = gui_.spb_FontSizeHighlited->value();

			curAppSetts.colorSetts_.freeColor = getColorName(gui_.btn_Free);
			curAppSetts.colorSetts_.occupiedColor = getColorName(gui_.btn_Occupied);
			curAppSetts.colorSetts_.closedColor = getColorName(gui_.btn_Closed);

			curAppSetts.wsSetts_.serverAddr_ = gui_.le_Address->text();
			curAppSetts.wsSetts_.serverPort_ = gui_.spb_Port->value();
			curAppSetts.wsSetts_.serverPath_ = gui_.le_Path->text();
			curAppSetts.scale_ = SettsSingle::instance()->getAppSetts().scale_;
			SettsSingle::instance()->setAppSetts(curAppSetts);

			wsClient_.updateParams(curAppSetts.wsSetts_);

			qt_utils::WidgetParams::restoreScrollBar(gui_.grv_Parking);

			for (auto pIt : parkGraphicsItems_) {
				pIt->setRotation(-SettsSingle::instance()->getAppSetts().rotateAngle_);
			}
			for (auto upIt : unitedParkGraphicsItems_) {
				upIt->setParkingRotation(-SettsSingle::instance()->getAppSetts().rotateAngle_);
				upIt->updateBounds();
			}
			setupMatrix();

			scene_->update();

			gui_.btn_Apply->setEnabled(false);
			gui_.btn_Cancel->setEnabled(false);
		}


		void MainWindow::colorButtonClicked() {
			if (QPushButton* curButton = qobject_cast<QPushButton*>(sender())) {
				const QColor initColor = curButton->palette().button().color();
				QColorDialog colorDlg(initColor, this);
				colorDlg.setWindowTitle(tr("Выберите цвет"));

				if (colorDlg.exec() == QDialog::Accepted) {
					QColor selectedColor = colorDlg.currentColor();
					QPalette selectedPalette;
					selectedPalette.setColor(curButton->backgroundRole(), selectedColor);
					curButton->setPalette(selectedPalette);

					settingsChanged();
				}
			}
		}


		void MainWindow::fontFamilyTextChanged(const QString& text) {
			QFont font(text);
			gui_.cb_FontFamily->setFont(font);
		}


		void MainWindow::zoomInButtonClicked() {
			if (SettsSingle::instance()->updateScale(0.5)) {
				setupMatrix();
			}
		}


		void MainWindow::zoomOutButtonClicked() {
			if (SettsSingle::instance()->updateScale(-0.5)) {
				setupMatrix();
			}
		}


		void MainWindow::parkingUpdated(const ParkingInfo& prk) {
			Q_EMIT wsSendTextMessage(CreateClientParkingChangeMsg(prk));
		}


		void MainWindow::parkingChangedReceived(const ParkingInfo& prk) {
			prkSourceModel_.updatePrk(prk); 
			
			for (auto prkIt : parkGraphicsItems_) {
				if (prkIt->getParkingName() == prk.name_) {
					prkIt->updateParkingInfo(prk);
					return;
				}
			}

			for (auto prkIt : unitedParkGraphicsItems_) {
				if (prkIt->getParkingNameContains(prk.name_)) {
					prkIt->updateParkingInfo(prk);
					return;
				}
			}			
		}


		void MainWindow::wsClientConnStateChanged(const bool &isConn) {
			statusBar()->showMessage(QString("Подключение к серверу: %1").arg(isConn ? "Подключено" : "Не подключено"));

			if (isConn) {
				Q_EMIT wsSendTextMessage(CreateRequestImageMsg());
			}
			else {
				resetScene();
			}
		}


		void MainWindow::parkingTableDoubleClicked(const QPoint& itemPos, const QPoint& screenPos) {
			
			const QModelIndex index = gui_.tblv_Parkings->indexAt(itemPos);

			if (index.isValid())
			{
				const QModelIndex sourceIndex = prkProxyModel_.mapToSource(index);
	
				ParkingDialog parkDlg(prkSourceModel_.getPrkByIndex(sourceIndex));
				parkDlg.move(screenPos);

				qt_utils::CheckWidgetPosition(&parkDlg);
				if (parkDlg.exec() == QDialog::Accepted) {
					parkingUpdated(parkDlg.GetParkingInfo());
				}				
			}
		}


		void MainWindow::initSettingsTab() {			
			gui_.spb_RotateAngle->setValue(SettsSingle::instance()->getAppSetts().rotateAngle_);
			gui_.spb_ParkingBorderWidth->setValue(SettsSingle::instance()->getAppSetts().borderWidth_);
			gui_.spb_ParkingSizeNormal->setValue(SettsSingle::instance()->getAppSetts().normalItemSize_);
			gui_.spb_ParkingSizeHighlited->setValue(SettsSingle::instance()->getAppSetts().highlightItemSize_);
			gui_.cb_FontFamily->setCurrentText(SettsSingle::instance()->getAppSetts().fontName_);
			gui_.spb_FontSizeNormal->setValue(SettsSingle::instance()->getAppSetts().normalFontSize_);
			gui_.spb_FontSizeHighlited->setValue(SettsSingle::instance()->getAppSetts().highlightFontSize_);
						
			setColorToButton(gui_.btn_Free, SettsSingle::instance()->getAppSetts().colorSetts_.freeColor);
			setColorToButton(gui_.btn_Occupied, SettsSingle::instance()->getAppSetts().colorSetts_.occupiedColor);
			setColorToButton(gui_.btn_Closed, SettsSingle::instance()->getAppSetts().colorSetts_.closedColor);

			gui_.le_Address->setText(SettsSingle::instance()->getAppSetts().wsSetts_.serverAddr_);
			gui_.spb_Port->setValue(SettsSingle::instance()->getAppSetts().wsSetts_.serverPort_);
			gui_.le_Path->setText(SettsSingle::instance()->getAppSetts().wsSetts_.serverPath_);

			gui_.btn_Apply->setEnabled(false);
			gui_.btn_Cancel->setEnabled(false);
		}


		void MainWindow::drawParkings(const ParkingInfoList &prkList) {
			resetScene();

			for (auto it : prkList) {
				if (it.unitedName_.isEmpty()) {
					parkGraphicsItems_.append(new ParkingGraphicsItem(it));
					scene_->addItem(parkGraphicsItems_.last());
					connect(parkGraphicsItems_.last(), &ParkingGraphicsItem::parkingInfoChanged,
						this, &MainWindow::parkingUpdated);
					parkGraphicsItems_.last()->setRotation(
						-SettsSingle::instance()->getAppSetts().rotateAngle_);
				}
			}

			UnitedParkingInfoList upList;

			for (int nn = prkList.size() - 1; nn >= 0; --nn) {
				if (!prkList.at(nn).unitedName_.isEmpty()) {

					UnitedParkingInfoList::iterator upIt = std::find_if(upList.begin(), upList.end(),
						[&](auto upIt) ->bool { return upIt.name_ == prkList.at(nn).unitedName_; });
					if (upIt == upList.end()) {
						UnitedParkingInfo up;
						up.name_ = prkList.at(nn).unitedName_;
						up.parkingList_.append(prkList.at(nn));
						upList.append(up);
					}
					else {
						upIt->parkingList_.append(prkList.at(nn));
					}
				}
			}

			for (auto unitedPrkIt : upList) {
				unitedParkGraphicsItems_.append(new UnitedParkingGraphicsItem(unitedPrkIt));
				scene_->addItem(unitedParkGraphicsItems_.last());
				connect(unitedParkGraphicsItems_.last(), &UnitedParkingGraphicsItem::parkingInfoChanged,
					this, &MainWindow::parkingUpdated);
				unitedParkGraphicsItems_.last()->setParkingRotation(
					-SettsSingle::instance()->getAppSetts().rotateAngle_);
			}
		}


		void MainWindow::setColorToButton(QPushButton* buttonPtr, const QString& colorName) {
			QPalette btnPalette;
			btnPalette.setColor(buttonPtr->backgroundRole(), QColor(colorName));
			buttonPtr->setPalette(btnPalette);
		}


		QString MainWindow::getColorName(QPushButton* buttonPtr) const {
			const QColor buttonColor = buttonPtr->palette().button().color();
			QString str = buttonColor.name();
			return buttonColor.name();
		}


		void MainWindow::settingsChanged() {
			gui_.btn_Apply->setEnabled(true);
			gui_.btn_Cancel->setEnabled(true);
		}


		void MainWindow::setupMatrix() {
			QMatrix matrix;
			matrix.scale(SettsSingle::instance()->getAppSetts().scale_, SettsSingle::instance()->getAppSetts().scale_);
			matrix.rotate(SettsSingle::instance()->getAppSetts().rotateAngle_);
			
			gui_.grv_Parking->setMatrix(matrix);

			gui_.tbtn_ZoomIn->setEnabled(SettsSingle::instance()->getAppSetts().scale_ < 7.0);
			gui_.tbtn_ZoomOut->setEnabled(SettsSingle::instance()->getAppSetts().scale_ > 1.0);			
		}

		void MainWindow::resetScene() {
			scene_->clear();
			parkGraphicsItems_.clear();
			unitedParkGraphicsItems_.clear();

			scene_->setSceneRect(0.0, 0.0, appronPixmap_.width(), appronPixmap_.height());
			scene_->addPixmap(appronPixmap_);		

			qt_utils::WidgetParams::restoreScrollBar(gui_.grv_Parking);
		}
	}
}